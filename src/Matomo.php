<?php

namespace KapibaraMedia\LaravelMatomo;

use Illuminate\Contracts\Support\Htmlable;

class Matomo implements Htmlable
{
    protected $matomo_url;
    protected $site_id;

    public function __construct()
    {
        $this->matomo_url = config('laravel-matomo.matomo_url');
        $this->site_id = config('laravel-matomo.site_id');
    }
    public function toHtml(): string
    {
        return view('laravel-matomo::matomo', [
            'matomo_url' => $this->matomo_url,
            'site_id' => $this->site_id,
        ]);
    }
}