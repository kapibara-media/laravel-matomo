<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    'site_id' => env('MATOMO_SITE_ID', 1),
    'matomo_url' => env('MATOMO_URL', 'matomo.kapibara.media')
];